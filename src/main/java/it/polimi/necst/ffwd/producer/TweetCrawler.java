package it.polimi.necst.ffwd.producer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Constants.FilterLevel;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import it.polimi.necst.ffwd.common.Settings;

public class TweetCrawler {

	public static void main(String[] args) throws IOException {
		
		ArrayList<String> keywords = Lists.newArrayList(Settings.real_keywords);
		BlockingQueue<String> queue =  new LinkedBlockingQueue<String>(10000);
	    StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();;
	    Client client;
		long oldTs = System.currentTimeMillis();
		long count = 0;
		long globalCount = 0;
		
		//connect to twitter streaming APIs
		endpoint.trackTerms(keywords);
		endpoint.filterLevel(FilterLevel.None);
		endpoint.languages(Lists.newArrayList(Settings.tweetLanguage));
		Authentication auth = new OAuth1(Settings.consumerKey, Settings.consumerSecret, Settings.accessToken, Settings.accessSecret);

		client = new ClientBuilder().hosts(Constants.STREAM_HOST).endpoint(endpoint).authentication(auth).processor(new StringDelimitedProcessor(queue)).build();
		client.connect();
		
		//open file for trace saving
		File file = new File(System.getProperty("user.dir") + File.separator + "tweets.txt");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		
		//get messages and send them to the rabbit queue
	    while(globalCount<5000000){
	    	JSONObject obj = getMessage(queue, client);
			if(obj!=null){
				
				try{
					long ts = System.currentTimeMillis();
					
					JSONObject save = new JSONObject();
					save.put("index", globalCount);
					save.put("sec_idex", count);
					
					save.put("keywords", getKeywords(obj, keywords));
					save.put("_source", obj);
					
					String saveToFile = save.toString();
		    		
					if(saveToFile!= null && bw!= null){
						bw.write(saveToFile + "\n");
						bw.flush();
					}
					
					globalCount++;
		    		count++;
		    		if(ts - oldTs>1000){
		    			System.out.println("Monitor: " + ((float)ts)/1000F + " " + count);
		    			oldTs = ts;
		    			count = 0;
		    		}
				}
				catch (Exception e){
					e.printStackTrace();
				}
				
	    	}
	    	
	    }
	    bw.close();
	}
	
	private static JSONObject getMessage(BlockingQueue<String> queue, Client client){
		try {
			String jsonTweet = queue.take();
			JSONObject obj = new JSONObject(jsonTweet);
			int i = 0;
			try{
				i = obj.getJSONObject("limit").getInt("track");
			}
			catch (JSONException e){
				
			}
			if(i!=0){
				System.out.println(i);
				return null;
			}
			return obj;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(client!=null){
				client.stop();
			}
			return null;
		}
	}
	
	private static String getText(JSONObject tweet){
		try{
			String text = tweet.getString("text");
			return text;
		}
		catch (Exception e){
			return "";
		}	
	}
	
	//This is potentially time consuming
	private static ArrayList<String> getKeywords(JSONObject tweet, ArrayList<String> keywords){
		ArrayList<String> matchedKeywords = new ArrayList<>();
		String text = getText(tweet);
		
		for(String s: keywords){
			boolean inside = Arrays.asList(text.toLowerCase().replace(".", "").replace(",", "").replace("?", "").replace("!","").replace("@","").replace("#","").split(" ")).contains(s.toLowerCase());
			
			if(inside){
				matchedKeywords.add(s);
			}
		}
		return matchedKeywords;
		
	}

}
