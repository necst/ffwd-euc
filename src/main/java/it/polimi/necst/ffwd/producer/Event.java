package it.polimi.necst.ffwd.producer;


import java.util.ArrayList;

import org.json.JSONObject;

public class Event{

	//producer stuff
	private long ts;
	private ArrayList<String> keywords;
	private JSONObject message;
	private ArrayList<String> hashTags;
	private String text;
	
	//consumer stuff
	private int sentimentScore;
	private boolean dropped;
	private boolean insidePlan;
	
	
	public Event(){
		this.keywords = null;
		this.message = null;
		this.hashTags = null;
		this.text = "";
		this.ts = 0;
		this.sentimentScore = 2;
		this.dropped = false;
		this.insidePlan = true;
	}
	
	public Event(JSONObject message) {
		super();
		this.keywords = new ArrayList<>();
		this.message = message;
		this.ts = System.currentTimeMillis();
		this.hashTags = new ArrayList<>();
		this.text = "";
		this.sentimentScore = 2;
	}

	public ArrayList<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	public JSONObject getMessage() {
		return message;
	}

	public void setMessage(JSONObject message) {
		this.message = message;
	}

	public ArrayList<String> getHashTags() {
		return hashTags;
	}

	public void setHashTags(ArrayList<String> hashTags) {
		this.hashTags = hashTags;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public int getSentimentScore() {
		return sentimentScore;
	}

	public void setSentimentScore(int sentimentScore) {
		this.sentimentScore = sentimentScore;
	}

	public boolean isDropped() {
		return dropped;
	}

	public void setDropped(boolean dropped) {
		this.dropped = dropped;
	}

	public boolean isInsidePlan() {
		return insidePlan;
	}

	public void setInsidePlan(boolean insidePlan) {
		this.insidePlan = insidePlan;
	}
	
	
}
