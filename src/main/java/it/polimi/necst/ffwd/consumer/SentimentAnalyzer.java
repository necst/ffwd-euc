package it.polimi.necst.ffwd.consumer;

import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import it.polimi.necst.ffwd.producer.Event;

public class SentimentAnalyzer {
	private Properties pipelineProps;
	private StanfordCoreNLP pipeline;
	
	public SentimentAnalyzer(){
		//init stanford stuff
		pipelineProps = new Properties();
        pipelineProps.setProperty("annotators", "tokenize, ssplit, pos, parse, sentiment");
        pipeline = new StanfordCoreNLP(pipelineProps);
	}
	
	
	public void doSentiment(Event task){
		
		int longest = 0;
		int mainSentiment = 0;
		
		
		String text = task.getText();
		text = removeUntokenizableChars(text);
		Annotation annotation = pipeline.process(text);
		
		for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
			Tree tree = sentence.get(SentimentAnnotatedTree.class);
			
            int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
            
            String partText = sentence.toString();
            
            if (partText.length() > longest) {
                mainSentiment = sentiment;
                longest = partText.length();
            }
		}
		
		task.setSentimentScore(mainSentiment);		
		
	}
	
	private String removeUntokenizableChars(String text){
		
		String tmp1 = text.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
		String tmp2 = tmp1.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]", "");
		
		return tmp2;
	}
	
	
}
