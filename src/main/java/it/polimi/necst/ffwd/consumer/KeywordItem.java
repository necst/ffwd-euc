package it.polimi.necst.ffwd.consumer;

import java.util.HashMap;
import java.util.Map.Entry;

public class KeywordItem {

	
	HashMap<String, HashtagItem> hashtagTable;
	private long count;
	private long drop;
	
	private float weighted_count;
	private float weighted_drop;
	
	private long[] scores;
	private long responseTime;
	
	private float old_weighted_count;
	private float old_weighted_drop;
	
	private long[] old_scores;
	
	public KeywordItem() {
		hashtagTable = new HashMap<>();
		scores = new long[5];
		responseTime = 0;
		count = 0;
		drop = 0;
		weighted_count = 0;
		weighted_drop = 0;
		
		old_weighted_count = 0;
		old_weighted_drop = 0;

	}
	
	public void putScore(int score, long responseTime, int weight){
		if(score> 0 && score<5){
			scores[score]++;
			count++;
			weighted_count += 1/(float)weight;
			this.responseTime+=responseTime;
		}
	}

	public void putDrop(long responseTime, int weight){
		drop++;
		weighted_drop += 1/(float)weight;
		this.responseTime+=responseTime;
	}
	
	public void putHashtagScore(String hashtag, int score, long responseTime){
		if(score> 0 && score<5){
			HashtagItem item = hashtagTable.get(hashtag);
			//TODO: change 1 with hashtag weight
			item.putScore(score, responseTime, 1);
		}
	}
	
	public long getCount() {
		return count;
	}

	public long getDrop() {
		return drop;
	}
	
	public float getWheightedCount() {
		return weighted_count;
	}

	public float getWeightedDrop() {
		return weighted_drop;
	}
	
	public long[] getScores() {
		return scores;
	}

	public long getResponseTime() {
		return responseTime;
	}
	
	public HashMap<String, HashtagItem> getHashtagTable() {
		return hashtagTable;
	}
	
	public void flushItem(){
		old_scores = scores;
		old_weighted_count = weighted_count;
		old_weighted_drop = weighted_drop;
		
		count = 0;
		drop = 0;
		scores = new long[5];
		responseTime = 0;
		weighted_count = 0;
		weighted_drop = 0;
		
		for(Entry<String, HashtagItem> s: hashtagTable.entrySet()){
			s.getValue().flushItem();
		}
	}

	//getters for controller data!
	public float get_old_weighted_count() {
		return old_weighted_count;
	}

	public float get_old_weighted_drop() {
		return old_weighted_drop;
	}

	public long[] get_old_scores() {
		return old_scores;
	}
	
	//TODO: to be changed with weighted one?
	public float getPerformance(){
		return (((float)responseTime)/((float)(count+drop)))/1000;
	}
	
	public String getAccuracyString(long stepCount, String key){
		String buffer = stepCount + " " + "0" + " " + key + " ";
		
		for(int i=0; i<scores.length; i++){
			buffer = buffer + scores[i] + " " + (((float) scores[i])/((float) count+drop))*100 + " ";
		}
		buffer = buffer + count + " " + drop;
		
		return buffer;
	}
	
	public void rescale(){
		long total = count + drop;

		for(int i=0; i<scores.length; i++ ){
			if(scores[i]!=0 && count !=0){
				scores[i] = (total*scores[i])/count;
			}
		}
	}
	
}
