package it.polimi.necst.ffwd.shedder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.common.Settings;
import it.polimi.necst.ffwd.consumer.CategoryContribution;
import it.polimi.necst.ffwd.consumer.SentimentAggregator;
import it.polimi.necst.ffwd.producer.Event;



public class FairKeywordPolicy extends SheddingPolicy{
	
	
	public FairKeywordPolicy(SentimentAggregator sAggregator, LoadFilter filter, FileLogger logger) {
		super(sAggregator, filter, logger);
	}
	
	@Override
	public void computePlan(long capacity, long throughput, float n_evt, float qos){
		
		//clear shedding plan table from old values
		filter.removeAll();
		
		ArrayList<CategoryContribution> orderedKeys = sAggregator.getKeywords();
		
		//keywords in increasing order
		Collections.sort(orderedKeys, new Comparator<CategoryContribution>(){
			@Override
			public int compare(CategoryContribution o1, CategoryContribution o2) {
				if(o1 == null || o2 == null){
					return 0;
				}
				if(o1.getTotal() - o2.getTotal() < 0){
					return -1;
				}
				else if(o1.getTotal() - o2.getTotal() == 0){
					return 0;
				}
				else{
					return 1;
				}
			}


		});
		
		int evaluated = 0;
		long missingEvt = capacity;

		//traverse in increasing order, so the non-assigned events can be reassigned to bigger event classes
		for(CategoryContribution c : orderedKeys){
			
			//number of events assigned for the current priority class
			float assignment = ((float)missingEvt)/(float)(orderedKeys.size()-evaluated);
			
			//impact of the input class on the stream
			float sImpact = ((float)(c.getCount() + c.getDrop()))/((float) throughput*qos);
			//estimated number of events at time t+1
			float estimatedEvt = n_evt*sImpact;
			//fair distribution of events
			float normalizedPriority = 1/(float)orderedKeys.size();
			
			//choose the min between mu_ci and mu_i
			float mu_ci = normalizedPriority*assignment;
			
			//if the assigned number of events are bigger than the estimated events
			//drop nothing and the remaining evt must be taken by the others
			float pct;
			if(mu_ci>estimatedEvt){
				pct = 0F;
				missingEvt -= estimatedEvt;
			}
			else{
				pct = (1F - mu_ci/estimatedEvt)*100;
				missingEvt -= mu_ci;
			}
			
			if(Settings.mapMetrics){
				//build the shedding plan for the categories inside the group
				for(int j=0; j<7; j++){
					filter.addConstraint(Settings.mapping[Integer.parseInt(c.getKey())][j], new FilterItem(Settings.mapping[Integer.parseInt(c.getKey())][j], pct, 0));
				}
			}
			else{
				//build the shedding plan for the single input category
				filter.addConstraint(c.getKey(), new FilterItem(c.getKey(), pct, 0));
			}
			
			logger.println(c.getKey() + " " + sImpact + " " + estimatedEvt + " " + pct + " " + assignment);
			evaluated++;
			
		}
		filter.setThreshold(1500, n_evt);
	}
	
	@Override
	public ArrayList<String> selectField(Event message) {
		return message.getKeywords();
	}

}
