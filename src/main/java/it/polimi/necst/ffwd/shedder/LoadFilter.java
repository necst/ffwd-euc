package it.polimi.necst.ffwd.shedder;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import it.polimi.necst.ffwd.common.Settings;
import it.polimi.necst.ffwd.producer.Event;

public class LoadFilter {
	private ConcurrentHashMap<String, FilterItem> filterMap;
	private Random r;
	private float threshold;
	private SheddingPolicy policy;
	private boolean batch;
	private AtomicLong globalCounter;
	private float expectedThroughput;
	private boolean limit;
	
	
	public LoadFilter(boolean batch){
		filterMap = new ConcurrentHashMap<>();
		r = new Random();
		threshold = 0;
		policy = null;
		this.batch = batch;
		globalCounter = new AtomicLong(0);
		expectedThroughput = 1000F;
		limit = false;
	}
	
	public void addConstraint(String key, FilterItem item){
		filterMap.put(key, item);
	}
	
	public void removeConstraint(String key){
		filterMap.remove(key);
	}
	
	public void removeAll(){
		filterMap = new ConcurrentHashMap<>();
	}
	
	public void setThreshold(float threshold, float expectedThroughput){
		this.threshold = threshold;
		this.expectedThroughput = expectedThroughput;
		this.globalCounter.set(0);
	}
	
	//main method of filter, checks if the event must be dropped or not
	//looking at the minimum probability of the matched items inside the
	//shedding plan computed by the policy
	public boolean accept(Event message){
		
		if(!Settings.doLoadShedding){
			message.setDropped(false);
			return true;
		}
		
		globalCounter.incrementAndGet();
		
		//if dropped enough, let the events go upstairs
		if(shouldStop() && isLimit()){
			return true;
		}
		
		//extract a number
		float test = r.nextFloat()*100 +1;
		//check values against filterMap
		FilterItem item = this.getMinProb(policy.selectField(message));
		//account drops to SentimentAggregator using message fields
		if(item.pct>=101){
			message.setInsidePlan(false);
			if(test>=threshold){
				message.setDropped(false);
				return true;
			}
			message.setDropped(true);
			return false;
		}
		else {
			message.setInsidePlan(true);
			if(test>=item.pct){
				message.setDropped(false);
				return true;
			}
			message.setDropped(true);
			return false;
		}
		
	}
	
	private FilterItem getMinProb(ArrayList<String> keywords){
		float min = 101;
		FilterItem ret = new FilterItem();
		ret.pct = min;
		
		try{
			for(String s: keywords){
				FilterItem item = filterMap.getOrDefault(s, ret);
				if(ret.pct>item.pct){
					ret = filterMap.get(s);
				}
			}
		}
		catch (NullPointerException e){
			//Rare occasion in which the consumer access the table when
			//the policy is updating it
			e.printStackTrace();
		}
		return ret;
	}	

	public void setPolicy(SheddingPolicy policy) {
		this.policy = policy;
	}
	
	public boolean sendToBatch(){
		return batch;
	}
	
	//check if we reached the maximum expected throughput
	//if yes, stop to drop things
	public boolean shouldStop(){
		if(expectedThroughput<0){
			return false;
		}
		return globalCounter.get() > expectedThroughput;
	}
	//check if drop is limited to a certain number of events
	public boolean isLimit() {
		return limit;
	}

	public void setLimit(boolean limit) {
		this.limit = limit;
	}
	
}
