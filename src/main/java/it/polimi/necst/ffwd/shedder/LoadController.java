package it.polimi.necst.ffwd.shedder;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.common.Settings;

public class LoadController {
	
	private float targetResponseTime;
	private LoadFilter filter;
	private SheddingPolicy policy;
	private FileLogger logger;
	//moving average stuff
	private long[] loadMA;
	private float[] coeffMA;
	private float[] lambdaMA;
	private int index;
	private int indexLambda;
	private boolean useMA;
	private boolean useLambdaMA;
	private static final int size = 5;
	private long oldQueue;
	
	public LoadController(float targetResponseTime, LoadFilter filter, SheddingPolicy policy, FileLogger logger){
		this.targetResponseTime = targetResponseTime;
		this.filter = filter;
		this.policy = policy;
		this.logger = logger;
		loadMA = new long[size];
		lambdaMA = new float[size];
		coeffMA = new float[]{0.2F, 0.2F, 0.2F, 0.2F, 0.2F};
		index = 0;
		indexLambda = 0;
		useMA = false;
		useLambdaMA = false;
		oldQueue = 0;

	}


	public void computeSheddingCoefficients(float meanResponseTime, long count, long drop, long queue, boolean useLambdaAverage){
		//If we received events in the last interval, compute the new throughput
		//otherwise do not drop anything
		if(count>0){
			//obtain arrival rate and throughput
			float mu = count + drop;
			float lambda = queue - oldQueue + mu;
			//compute error in response time
			float error = meanResponseTime - targetResponseTime;
			
			//compute lambda average if requested
			float arrival;
			if(useLambdaAverage){
				arrival = computeLambdaAverage(lambda, queue);
			}
			else{
				arrival = lambda;
			}
			//compute the control action
			float action = arrival + arrival*error;
			
			//adjust signs
			float positiveAction = action>0? action : 1;
			
			//compute general drop percentage
			float pct = 0;
			if(count>0){
				pct = (1 - (count/positiveAction))*100;
			}
			//set limit to the drop action
			if(pct>0){
				filter.setLimit(true);
			}
			else{
				filter.setLimit(false);
			}
			
			logger.println("Controller: "+ pct +  " " + error + " " + action + " " + lambda + " " + mu + " " + queue + " " + meanResponseTime);
			//save queue length for next iteration
			oldQueue = queue;
			//set standard drop probability
			filter.setThreshold(pct, action);
			
			//compute capacity moving average to estimate the system capacity based on old data
			float capacity;
			if(Settings.useCapacityMA){
				capacity = computeMuAverage(count, queue);
			}
			else{
				capacity = count;
			}
			//compute drop plan for each plugged policy
			policy.computePlan((long)capacity, count+drop, positiveAction, targetResponseTime);
		}
		else{
			filter.setThreshold(0, 0);
			filter.setLimit(false);
		}
		
	}
	
	
	private float computeLambdaAverage(float lambda, long queue){
		float arrival = lambda;
		//lambda MA with ring buffer
		if(queue>0){
			indexLambda++;
			if(indexLambda>size-1){
				indexLambda = 0;
			}
			
			lambdaMA[indexLambda] = lambda;
			if(useLambdaMA){
				arrival = 0;
				for(int i=0; i<size; i++){
					int eff = indexLambda - i;
					if(eff<0){
						eff = size + indexLambda - i;
					}
					arrival += coeffMA[i]*lambdaMA[eff];
				}
			}
			else{
				if(!useLambdaMA && indexLambda==size-1){
					useLambdaMA = true;
					arrival = ((float)(coeffMA[4]*lambdaMA[0]+coeffMA[3]*lambdaMA[1]+coeffMA[2]*lambdaMA[2]+coeffMA[1]*lambdaMA[3]+coeffMA[0]*lambdaMA[4]));
				}
			}
		}
		
		return arrival;
	}
	
	private float computeMuAverage(long count, long queue){
		float capacity = count;
		//capacity MA with ring buffer
		if(queue>0){
			index++;
			if(index>size-1){
				index = 0;
			}
			
			loadMA[index] = count;
			if(useMA){
				capacity = 0;
				for(int i=0; i<size; i++){
					int eff = index - i;
					if(eff<0){
						eff = size + index - i;
					}
					capacity += coeffMA[i]*loadMA[eff];
				}
			}
			else{
				if(!useMA && index==size-1){
					useMA = true;
					capacity = (long)((float)(coeffMA[4]*loadMA[0]+coeffMA[3]*loadMA[1]+coeffMA[2]*loadMA[2]+coeffMA[1]*loadMA[3]+coeffMA[0]*loadMA[4]));
				}
			}
		}
		
		return capacity;
		
	}
	
	
}
