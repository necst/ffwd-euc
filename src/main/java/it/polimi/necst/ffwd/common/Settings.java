package it.polimi.necst.ffwd.common;

public class Settings {
	//
	// Producer Settings
	//

	//RabbitMQ queue name for communication between consumer(s) and producer(s)
	public static final String REAL_TIME_QUEUE = "tweet_queue";
	//twitter API settings (insert here account and application codes)
	public static final String accessToken = "";
	public static final String accessSecret = "";
	public static final String consumerKey = "";
	public static final String consumerSecret = "";
	public static final String tweetLanguage = "en";
	
	//input type
	public static final String state = "test";
	//public static final String state = "fake";
	//public static final String state = "real";
	
	//input step or ramp for controller tests
	public static final boolean step = true;
	//input keywords for synthetic tweets and related priorities
	//useful only to test functionality of policies
	public static final String[] fake_keywords = {"message"};
	public static final int[] fake_priorities = {5};
	//input keywords for real data from Twitter APIs and related priorities
	public static final String[] real_keywords = {"manchester", "stoke", "bournemouth", "chelsea", "aston", "southampton", "west", "liverpool", "newcastle", "premier", "mcfc", "manutd", "mufc", "stokecity",
			"afcbournemouth", "chelseafc", "avfcofficial", "southamptonfc", "wbafcofficial", "lfc", "nufc", "premierleague", "leicester", "lcfc", "swansea", "arsenal", "tottenham", "spursofficial"};
	public static final int[] real_priorities = {5,3,2,5,1,1,4,5,3,2,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3};
	
	//
	// Consumer settings
	//

	//Collect data flags
	public static final boolean verbose = true;
	public static final boolean log = true;
	public static final boolean collectResults = true;

	//Number of consumer threads
	public static final int consumerThreads = 8;

	// Static setting for FFWD policies
	// 0 -> Baseline
	// 1 -> PriorityBasedPolicy (keywords)
	// 2 -> FairKeyword
	// 3 -> FairHashtag
	public static final int policy = 1;
	//Send to batch queue flag
	public static final boolean sendToBatch = false;
	//use moving average to estimate mu_c(t+1)
	public static final boolean useCapacityMA = false;
	//use moving average to estimate lambda(t+1)
	public static final boolean useLambdaAverage = false;
	//limit dropping action to the estimated mu(t), after that, everything is computed
	//until new control step
	public static final boolean limit = true;
	//static setting for timing constraints
	public static final int qos = 5;
	//do load shedding, false to build the exact trace of metrics (with sensible delays)
	public static final boolean doLoadShedding = true;
	
	public static final boolean mapMetrics = false;
	//static mapping for paper tests with related priorities
	public static final String[][] mapping = {{"manchester","stoke","mcfc","manutd","mufc","stokecity","swansea"},{"bournemouth","chelsea","afcbournemouth", "chelseafc","premierleague","premier","arsenal"},
			{"aston", "southampton", "west", "liverpool", "newcastle","southamptonfc", "lfc"},{"nufc","leicester", "lcfc","tottenham", "spursofficial","wbafcofficial","avfcofficial"}};
	public static final int[] mappingPriority = {4,3,2,1};
	
}
